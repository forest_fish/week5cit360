package LearnJUnit;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PetDogTest {
    private PetDog mookie = new PetDog("Mookie");
    private PetDog mookie2 = mookie;
    private int test = 2000;

    /*
    * Demonstrate the assertSame, assertNotSame, and the assertThat with instanceOf methods.
    * */
        @Test
    public void testMookie(){
        assertThat(mookie, instanceOf(PetDog.class));
        assertSame(mookie, mookie2);
        assertNotSame(mookie, test);
    }

    /*
    * Demonstrate the assertEquals methods.
    */
    @Test
    public void getName(){
        assertEquals("Mookie", mookie.getName());
    }

    /*
    * Demonstrate the asterTrue and assertFalse methods.
    */
    @Test
    public void testHappy() {
        assertTrue(mookie.isHappy());
        mookie.setHappy(false);
        assertFalse(mookie.isHappy());
    }

    /*
    * Demonstrate the assertThat method with various matchers.
    */
    @Test
    public void isHungry() {
        assertThat(mookie.isHungry(), isA(Boolean.class));
        assertThat(mookie.isHungry(), is(false));
        assertThat(mookie.isHungry(), not(true));
        mookie.setHungry(true);
        assertThat(mookie.isHungry(), not(false));
        assertThat(mookie.isHungry(), is(true));
    }

    /*
    * Demonstrate the assertArrayEquals, assertNotNull, and assertNull methods.
     */
    @Test
    public void whatTricks() {
        String[] tricks = {"sit", "lay down", "shake", "roll over"};
        assertArrayEquals(tricks, mookie.getTricks());
        assertNotNull(mookie.getTricks());
        mookie.setTricks(null);
        assertNull(mookie.getTricks());
    }
}