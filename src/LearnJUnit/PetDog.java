package LearnJUnit;

public class PetDog {
    private String name;
    private boolean happy = true;
    private boolean hungry = false;
    private String[] tricks = {"sit", "lay down", "shake", "roll over"};


    public PetDog(String name){
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHappy() {
        return happy;
    }

    public void setHappy(Boolean happy) {
        this.happy = happy;
    }

    public String[] getTricks() {
        return tricks;
    }

    public void setTricks(String[] tricks){
        this.tricks = tricks;
    }

    public boolean isHungry() {
        return hungry;
    }

    public void setHungry(boolean hungry) {
        this.hungry = hungry;
    }
}
